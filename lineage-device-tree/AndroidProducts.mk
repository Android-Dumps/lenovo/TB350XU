#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TB350XU.mk

COMMON_LUNCH_CHOICES := \
    lineage_TB350XU-user \
    lineage_TB350XU-userdebug \
    lineage_TB350XU-eng
